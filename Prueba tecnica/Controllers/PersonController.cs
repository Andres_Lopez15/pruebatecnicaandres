﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Prueba_tecnica.Models;
using System.Security.Cryptography;
using System.Text;

namespace Prueba_tecnica.Controllers
{
    public class PersonController : Controller
    {
        Entities entidad = new Entities();
        public int Id;
        private string hash;

        public static string GetMD5(string str)
        {
            MD5 md5 = MD5CryptoServiceProvider.Create();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] stream = null;
            StringBuilder sb = new StringBuilder();
            stream = md5.ComputeHash(encoding.GetBytes(str));
            for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
            return sb.ToString();
        }

        public int GetUsers(string Email)
        {
            using (var db = new Entities())
            {
                var query = (from b in db.Person where b.Email == Email select b).Count();
                return query;
            }
        }

        public ActionResult Register(string Name, string Email, string Contra)
        {
            hash = GetMD5(Contra);
            var conteo = GetUsers(Email);

            if (conteo > 0)
            {
                @ViewBag.Error = "Ya existe un usuario con ese correo";
                return View("Error");
            }
            else
            {
                using (var db = new Entities())
                {
                    db.Person.Add(new Person
                    {
                        Name = Name,
                        Email = Email,
                        Pass = hash
                    });

                    db.SaveChanges();
                }
                @ViewBag.message = "El usuario se registro correctamente";
            }
            return View("Successful");
        }

        public ActionResult Login(string Email, string Contra)
        {
            var password = GetMD5(Contra);

            using (var db = new Entities())
            {
                var query = (from p in db.Person where p.Email == Email && p.Pass == password select p).ToList();

                foreach (var item in query)
                {
                    Id = item.Id;
                    if (Id != 0)
                    {
                        Session["IdPerson"] = item.Id;
                        Session["Name"] = item.Name;
                        Session["Email"] = item.Email;

                        return View("User");
                    }
                    else
                    {
                        @ViewBag.Error = "No se encontro el usuario";
                        return View("Error");
                    }
                } 
            }
            @ViewBag.Error = "No se encontro usuario intente de nuevo";
            return View("Error");
        }
        
        public ActionResult Ingresos()
        {
            return View("Incomes");
        }

        public ActionResult SendIncomes(int sueldo, int bonos, int otros, int total, DateTime? Time, int PersonId)
        {
            using (entidad)
            {
                if (sueldo != 0)
                {
                    entidad.IncomeExpenses.Add(new IncomeExpenses
                    {
                        Value = sueldo,
                        IdCategory = 9,
                        Time = Time,
                        IdPerson = PersonId,
                        IncExp = 2
                    });
                }
                if (bonos != 0)
                {
                    entidad.IncomeExpenses.Add(new IncomeExpenses
                    {
                        Value = bonos,
                        IdCategory = 10,
                        Time = Time,
                        IdPerson = PersonId,
                        IncExp = 2
                    });
                }
                if (otros != 0)
                {
                    entidad.IncomeExpenses.Add(new IncomeExpenses
                    {
                        Value = otros,
                        IdCategory = 11,
                        Time = Time,
                        IdPerson = PersonId,
                        IncExp = 2
                    });
                }
                

                entidad.SaveChanges();

            }
            ViewBag.Message = "Los ingresos se guardaron correctamente";
            return View("Successful");
        }

        public ActionResult Gastos()
        {
            return View("Expenses");
        }

        public ActionResult SendExpenses(int? alimentacion, int? cuentas, int? casa, int? transporte, int? vestuario, int? salud, int? diversion, int? otros, DateTime? time, int personId)
        {
            using (entidad)
            {
                if (alimentacion != 0)
                {
                    entidad.IncomeExpenses.Add(new IncomeExpenses
                    {
                        Value = alimentacion,
                        IdCategory = 1,
                        Time = time,
                        IdPerson = personId,
                        IncExp = 1

                    });
                }
                if (cuentas != 0)
                {
                    entidad.IncomeExpenses.Add(new IncomeExpenses
                    {
                        Value = cuentas,
                        IdCategory = 2,
                        Time = time,
                        IdPerson = personId,
                        IncExp = 1
                    });
                }
                if (casa != 0)
                {
                    entidad.IncomeExpenses.Add(new IncomeExpenses
                    {
                        Value = casa,
                        IdCategory = 3,
                        Time = time,
                        IdPerson = personId,
                        IncExp = 1
                    });
                }

                if (transporte != 0)
                {
                    entidad.IncomeExpenses.Add(new IncomeExpenses
                    {
                        Value = transporte,
                        IdCategory = 4,
                        Time = time,
                        IdPerson = personId,
                        IncExp = 1
                    });
                }
                if (vestuario != 0)
                {
                    entidad.IncomeExpenses.Add(new IncomeExpenses
                    {
                        Value = vestuario,
                        IdCategory = 5,
                        Time = time,
                        IdPerson = personId,
                        IncExp = 1

                    });
                }
                if (salud != 0)
                {
                    entidad.IncomeExpenses.Add(new IncomeExpenses
                    {
                        Value = salud,
                        IdCategory = 6,
                        Time = time,
                        IdPerson = personId,
                        IncExp = 1
                    });
                }
                if (diversion != 0)
                {
                    entidad.IncomeExpenses.Add(new IncomeExpenses
                    {
                        Value = diversion,
                        IdCategory = 7,
                        Time = time,
                        IdPerson = personId,
                        IncExp = 1
                    });
                }
                if (otros != 0)
                {
                    entidad.IncomeExpenses.Add(new IncomeExpenses
                    {
                        Value = otros,
                        IdCategory = 8,
                        Time = time,
                        IdPerson = personId,
                        IncExp = 1
                    });
                }



                entidad.SaveChanges();

            }
            ViewBag.Message = "Los gastos se guardaron correctamente";
            return View("Successful");
        }

        public ActionResult Informe()
        {
           return RedirectToAction("GetReport");
        }

        public ActionResult GetReport(DateTime? firstDate, DateTime? LastDate, int? personid)
        {
            if (firstDate != null || LastDate != null || personid != null)
            {
                using (var db = new Entities())
                {
                    var query = (from p in db.IncomeExpenses.Include("Category")
                        where p.Time >= firstDate && p.Time <= LastDate && p.IdPerson == personid
                        select p);

                    return View(query.ToList());
                }
            }
            else
            {
                firstDate = DateTime.Now;
                LastDate = DateTime.Now;
                using (var db = new Entities())
                {
                    var query =
                    (from p in db.IncomeExpenses
                     where p.Time >= firstDate && p.Time <= LastDate && p.IdPerson == personid
                     select p);

                    return View(query.ToList());
                }
            }
        }

        public ActionResult User()
        {
            return View();
        }

        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("Index","Home");
        }

        public ActionResult Options()
        {
            return View("Options");
        }

        public ActionResult ChangeName(string Name,string password,int PersonId)
        {
            using (var db = new Entities())
            {
                string contra = GetMD5(password);
                var query = (from p in db.Person where p.Id == PersonId && p.Pass == contra select p).Count();

                if (query > 0)
                {
                    Person P = db.Person.Find(PersonId);

                    P.Name = Name;
                    db.SaveChanges();
                    ViewBag.Message = "Se cambio el nombre correctamente";
                    return View("Successful");
                }
                else
                {
                    ViewBag.Error = "No se encontro el usuario o esta incorrecta la contraseña";
                    return View("Error");
                }

                
            }
        }

        public ActionResult ChangePassword(string NewPassword, string password, int PersonId)
        {
            using (var db = new Entities())
            {
                string contra = GetMD5(password);
                string newcontra = GetMD5(NewPassword);
                var query = (from p in db.Person where p.Id == PersonId && p.Pass == contra select p).Count();

                if (query > 0)
                {
                    Person P = db.Person.Find(PersonId);

                    P.Pass = newcontra;
                    db.SaveChanges();
                    ViewBag.Message = "Se modifico correctamente la contraseña";
                    return View("Successful");
                }
                else
                {
                    ViewBag.Error = "No se encontro el usuario o esta incorrecta la contraseña";
                    return View("Error");
                }


            }
        }

        public ActionResult ModifyName()
        {
            return View("ModifyName");
        }

        public ActionResult ModifyPassword()
        {
            return View("ModifyPassword");
        }

        public ActionResult DelField(int FieldId)
        {
            IncomeExpenses p = entidad.IncomeExpenses.Find(FieldId);
            entidad.IncomeExpenses.Remove(p);
            entidad.SaveChanges();

            ViewBag.Message = "Se elimino el registro";
            return View("Successful");
        }
    }
}