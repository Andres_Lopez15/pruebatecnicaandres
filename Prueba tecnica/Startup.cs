﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Prueba_tecnica.Startup))]
namespace Prueba_tecnica
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
