//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Prueba_tecnica.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Incomes
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public Nullable<int> IdCategory { get; set; }
        public Nullable<System.DateTime> Time { get; set; }
        public Nullable<int> IdPerson { get; set; }
    
        public virtual Category Category { get; set; }
        public virtual Person Person { get; set; }
    }
}
